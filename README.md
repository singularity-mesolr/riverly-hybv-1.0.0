# RIVERLY-HYBV-1.0.0

Projet HYBV pour l'unité RIVERLY de Lyon. Ce projet embarque GRASS, HRU-DELIN et JAMS

## BUILD
```bash
singularity build riverly-hybv-1.0.0.sif riverly-hybv-1.0.0.def
```

## LOCAL USAGE on MESOLR
```bash
module load singularity/3.5
singularity pull riverly-hybv-1.0.0.sif oras://registry.forgemia.inra.fr/singularity-mesolr/riverly-hybv-1.0.0/riverly-hybv-1.0.0:latest

# Download SVN
singularity run --app svn riverly-hybv-1.0.0.sif co --username 'guest' --password 'guest' http://svn.geogr.uni-jena.de/svn/modeldata/J2K_Gehlberg 

# Run JAMS, you MUST put absolute path after "run"
singularity exec riverly-hybv-1.0.0.sif bash -c "cd /opt/jams/trunk/jams-bin && java -Xms128M -Xmx1024M -splash: -jar jams-starter.jar $HOME/J2K_Gehlberg/j2k_gehlberg.jam"
```

## SLURM USAGE on MESOLR
- Go to scratch
- Create a folder here it is named "FOLDER"
- Go to this folder
- Create "logs" folder
- Create a list of jam files like the example below named simulation.txt
```bash
/home/verrierj/J2K_Gehlberg/j2k_gehlberg1.jam
/home/verrierj/J2K_Gehlberg/j2k_gehlberg2.jam
/home/verrierj/J2K_Gehlberg/j2k_gehlberg3.jam
/home/verrierj/J2K_Gehlberg/j2k_gehlberg4.jam
/home/verrierj/J2K_Gehlberg/j2k_gehlberg5.jam
/home/verrierj/J2K_Gehlberg/j2k_gehlberg6.jam
/home/verrierj/J2K_Gehlberg/j2k_gehlberg7.jam
/home/verrierj/J2K_Gehlberg/j2k_gehlberg8.jam
/home/verrierj/J2K_Gehlberg/j2k_gehlberg9.jam
/home/verrierj/J2K_Gehlberg/j2k_gehlberg10.jam
```
- Create a batch file with the following contents named SOMETHING.batch
```bash
#! /usr/bin/env bash

## PARAMS
#SBATCH -A inrae-infra
#SBATCH -p defq
# Rediriger les erreurs et sorties dans /lustre/$USER
#SBATCH --output=/lustre/$USER/FOLDER/logs/array-%a.out
#SBATCH --error=/lustre/$USER/FOLDER/logs/array-%a.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=prenom.nom@inrae.fr
#SBATCH --mem=1G

## 1 CORE, 1 JOB
#SBATCH -c 1

## 1 day max for the job
#SBATCH --time=1-00:00:00

## ARRAY
## ARRAY launch 10 jobs from 1 to 10 (1-10). Only 5 (%5) jobs can run concurrently.
#SBATCH --array=1-10%5


module load singularity/3.5

echo $SLURM_ARRAY_TASK_ID
VAR=$(sed -n ${SLURM_ARRAY_TASK_ID}p simulation.txt)

singularity exec riverly-hybv-1.0.0.sif bash -c "cd /opt/jams/trunk/jams-bin && java -Xms128M -Xmx1024M -splash: -jar jams-starter.jar $VAR"
```
- Launch batch file with sbatch `SOMETHING.batch`
